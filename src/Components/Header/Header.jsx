import './Header.scss'

//Router-dom
import { Link } from 'react-router-dom'

export const Header = () => {
    return (<div className="c-header">
        <nav className="c-header__nav">
            <Link to="/" className="c-header__nav__link">Home</Link> 
            <Link to="/clinic" className="c-header__nav__link">Worked Up Code</Link> 
            <Link to="/workers" className="c-header__nav__link">Proyectos</Link>  
            <Link to="/center" className="c-header__nav__link">Contacto</Link> 
        </nav>
        
        </div>)
}
